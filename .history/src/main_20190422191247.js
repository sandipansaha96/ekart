import Vue from 'vue'
import App from './App.vue'

import firebase from 'firebase'
import './components/firebaseInit'

Vue.config.productionTip = false


//import router from './router'
import router from './router'
Vue.use(router)



import Vuex from 'vuex'
Vue.use(Vuex)

import { storage } from './store'
const store = new Vuex.Store(storage)

let app;
firebase.auth().onAuthStateChanged(user=>{
  if(!app){
    app=new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  }
});

