import Vue from 'vue'
import App from './App.vue'

import firebase from 'firebase'

Vue.config.productionTip = false

import VueRouter from 'vue-router'
Vue.use(VueRouter)

//import router from './router'
import { routes } from './router'

const router = new VueRouter({
  mode: 'history',
  routes
})
router.beforeEach((to, from, next) => {
  //check for requiresAuth guard
  if (to.matched.some(record => record.meta.requiresAuth)) {
      //check if not logged in in firebase
      if (!firebase.auth().currentUser) {
          //go to login page
          next({
              path: '/login',
              // query: {
              //     redirect: to.fullpath
              //}
          });
      } else {
          //proceed to route by calling next
          next();
      }
  }
  else if (to.matched.some(record => record.meta.requiresguest)) {
      //check if  logged in in firebase
      if (firebase.auth().currentUser) { //check if not logged in in firebase
          if (!firebase.auth().currentUser) {
              //go to login page
              next({
                  path: '/',
                  // query: {
                  //     redirect: to.fullpath
                  // }
              });
          }
          else {
              //proceed to route by calling next
              next();
          }
      }
  }
  else {
      next();
  }
}) 


import Vuex from 'vuex'
Vue.use(Vuex)

import { storage } from './store'
const store = new Vuex.Store(storage)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
