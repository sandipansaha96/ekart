import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Login from './components/Login'
import Register from './components/Register'

export const routes = [{
    path: '',
    component: Inventory,

},
{
    path: '/item/:id',
    component: ItemDetails,

},
{
    path: '/login',
    component: Login,

},
{
    path: '/register',
    component: Register,

}
]