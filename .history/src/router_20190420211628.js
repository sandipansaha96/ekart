import VueRouter from 'vue-router'
Vue.use(VueRouter)

import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Login from './components/Login'
import Register from './components/Register'
import Dashboard from './components/Dashboard'
import firebase from 'firebase'



const router = new VueRouter({
    mode: 'history',
    routes: [{
        path: '',
        component: Inventory,
        meta: {
            requiresAuth: true
        }

    },
    {
        path: '/item/:id',
        component: ItemDetails,
        meta: {
            requiresAuth: true
        }


    },
    {
        path: '/login',
        component: Login,
        meta: {
            requiresguest: true
        }


    },
    {
        path: '/register',
        component: Register,
        meta: {
            requiresguest: true
        }


    },
    {
        path: '/dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true
        }


    }
    ]
})

router.beforeEach((to, from, next) => {
    //check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {
        //check if not logged in in firebase
        if (!firebase.auth().currentUser) {
            //go to login page
            next({
                path: '/login',
                query: {
                    redirect: to.fullpath
                }
            });
        } else {
            //proceed to route by calling next
            next();
        }
    }
    else if (to.matched.some(record => record.meta.requiresguest)) {
        //check if  logged in in firebase
        if (firebase.auth().currentUser) { //check if not logged in in firebase
            if (!firebase.auth().currentUser) {
                //go to login page
                next({
                    path: '/',
                    query: {
                        redirect: to.fullpath
                    }
                });
            }
            else {
                //proceed to route by calling next
                next();
            }
        }
    }
    else {
        next();
    }
});
export default router;

