import firebase from 'firebase'
export const storage = {
    state: {
        inventory: [],
        cart: [],
        user: null,
        isAuthenticated: false,
    },
    getters: {
        getInventory(state) {
            return state.inventory
        },
        getCart(state) {
            return state.cart
        },
        isAuthenticated(state){
            return state.user !==null && state.user !==undefined;
        }
    },
    mutations: {
        setInventory(state, payload) {
            state.inventory = payload
        },
        addToCart(state, payload) {
            state.cart.push(payload)
        },
        removeItem(state, payload) {
            state.cart.splice(payload, 1)
        },
        clearCart(state) {
            state.cart = []
        },
        setUser(state, payload) {
            state.user = payload;
        },
        setAuthenticated(state, payload) {
            state.isAuthenticated = payload;
        },
    },
    actions: {
        addToCartAction(context, payload) {
            context.commit('addToCart', payload)
        },
        Registration({ commit }, { email, password,router }) {
            firebase.auth().createUserWithEmailAndPassword(email,password)
                .then(user => {
                    commit('setUser', user);
                    commit('setAuthenticated', true);
                    alert(`Account created ${user.email}.`);
                    router.push("/login");
                    //router.go({ path: router.path });
                })
                .catch(() => {
                    commit('setUser', null);
                    commit('setAuthenticated', false);
                });
        },
        Login({ commit }, { email, password,router }) {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(user => {
                    commit('setUser', user);
                    commit('setAuthenticated', true);
                    alert(`You are logged in as  ${user.email}`);
                    router.push("/");
                    //router.go({ path: router.path });
                })
                .catch(() => {
                    commit('setUser', null);
                    commit('setAuthenticated', false);
                }
                );
        }
    }
}