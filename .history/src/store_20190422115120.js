import router from '@/router';

export const storage = {
  state: {
      inventory: [],
      cart: []
  },
  getters: {
      getInventory(state) {
          return state.inventory
      },
      getCart(state) {
          return state.cart
      }
  },
  mutations: {
      setInventory(state, payload) {
          state.inventory = payload
      },
      addToCart(state, payload) {
          state.cart.push(payload)
      },
      removeItem(state, payload) {
          state.cart.splice(payload, 1)
      },
      clearCart(state) {
          state.cart = []
      },
  },
  actions:{
      addToCartAction(context,payload)
      {
          context.commit('addToCart',payload)
      },
      Registration({commit},{email,password}){
        firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
        .then(user =>{
              commit('setUser',user);
              commit('setAuthenticated',true);
              alert(`Account created ${user.email}.`);
              //router.push("/");
              router.go({path:this.$router.path});
            },
            err => {
              alert(err.message);
            }
          );
      }
  }
}