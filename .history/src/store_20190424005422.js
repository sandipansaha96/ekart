import firebase from 'firebase'
import router from './router'
export const storage = {
    state: {
        inventory: [],
        cart: [],
        user: null,
        isAuthenticated: false,
        isLoggedIn:false,

    },
    getters: {
        getInventory(state) {
            return state.inventory
        },
        getCart(state) {
            return state.cart
        },
        getSearchedInventory()
        {
            state.inventory=state.inventory.filter(item => {
                //return item.title.match(keyword)
                return item.title.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
              });

        },
        isAuthenticated(state){
            return state.user !=null && state.user !=undefined;
        }
    },
    mutations: {
        setInventory(state, payload) {
            state.inventory = payload
        },
        addToCart(state, payload) {
            state.cart.push(payload)
        },
        removeItem(state, payload) {
            state.cart.splice(payload, 1)
        },
        clearCart(state) {
            state.cart = []
        },
        setUser(state, payload) {
            state.user = payload;
        },
        setAuthenticated(state, payload) {
            state.isAuthenticated = payload;
        },
        setLoggedIn(state, payload) {
            state.isLoggedIn = payload;
        }
    },
    actions: {
        addToCartAction(context, payload) {
            context.commit('addToCart', payload)
        },
         async Registration({ commit }, { email, password }) {
            firebase.auth().createUserWithEmailAndPassword(email,password)
                .then(user => {
                    commit('setUser', user);
                    commit('setAuthenticated', true);
                    commit('setLoggedIn', false);
                    router.push("/login");
                    M.toast({ html: "Registered Successfully" });
                })
                .catch(() => {
                    commit('setUser', null);
                    commit('setAuthenticated', false);
                    commit('setLoggedIn', false);
                    M.toast({ html: "Registeration Failed" });

                });
        },
         async Login({ commit }, { email, password }) {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(user => {
                    commit('setUser', user);
                    commit('setAuthenticated', true);
                    commit('setLoggedIn', true);
                    router.push("/");
                    M.toast({ html: "Logged in Successfully" });                 
                    
                })
                .catch(() => {
                    commit('setUser', null);
                    commit('setAuthenticated', false);
                    commit('setLoggedIn', false);
                    M.toast({ html: "Unsuccessfully" });

                }
                );
        }
    }
}