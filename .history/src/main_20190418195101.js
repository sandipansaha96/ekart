import Vue from 'vue'
import App from './App.vue'



Vue.config.productionTip = false

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import { routes } from './router'

const router = new VueRouter({
  mode: 'history',
  routes
})

import Vuex from 'vuex'
Vue.use(Vuex)

import {storage} from './store'
const store=new Vuex.Store(storage)

new Vue({
  el:'#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
