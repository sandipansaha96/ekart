import firebase from 'firebase'
import 'firebase/firestore'
import firebaseConfig from './firebaseConfig'

const firebaseApp=firebase.initializeApp(firebaseConfig)

export const db = firebaseApp.database()
export const namesRef = db.ref('0/data') 
export default firebaseApp.firestore()
