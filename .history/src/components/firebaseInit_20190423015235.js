import firebase from 'firebase'
import 'firebase/firestore'
import firebaseConfig from './firebaseConfig'

const firebaseApp=firebase.initializeApp(firebaseConfig)

export const db = firebaseApp.database()
export const dataRef = db.ref('data')
export default firebaseApp.firestore()
