import firebase from 'firebase'
import 'firebase/firestore'
import firebaseConfig from './firebaseConfig'

const firebaseApp=firebase.initializeApp(firebaseConfig)

export const db = firebaseApp.database()
export const dataRef = db.ref('0/data').val() 
export default firebaseApp.firestore()
