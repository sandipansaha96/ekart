import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Login from './components/Login'
import Register from './components/Register'
import Dashboard from './components/Dashboard'
//import firebase from 'firebase'

export const routes =  [{
    path: '',
    component: Inventory,
    meta: {
        requiresAuth: true
    }

},
{
    path: '/item/:id',
    component: ItemDetails,
    meta: {
        requiresAuth: true
    }


},
{
    path: '/login',
    component: Login,
    meta: {
        requiresguest: true
    }


},
{
    path: '/register',
    component: Register,
    meta: {
        requiresguest: true
    }


},
{
    path: '/dashboard',
    component: Dashboard,
    meta: {
        requiresAuth: true
    }


}
];


