import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Login from './components/Login'
import Register from './components/Register'
import Dashboard from './components/Dashboard'
import firebase from 'firebase'

export const routes = [{
    path: '',
    component: Inventory,
    meta:{
        requiresAuth:true
    }

},
{
    path: '/item/:id',
    component: ItemDetails,

},
{
    path: '/login',
    component: Login,

},
{
    path: '/register',
    component: Register,

},
{
    path: '/dashboard',
    component: Dashboard,

}
]