import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Pod from './components/views/Pod'

export const routes = [{
    path: '',
    component: Inventory,

},
{
    path: '/item/:id',
    component: ItemDetails,

}
]