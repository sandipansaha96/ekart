import ItemDetails from './components/views/ItemDetails'
import Inventory from './components/views/Inventory'
import Login from './components/Login'


export const routes = [{
    path: '',
    component: Inventory,

},
{
    path: '/item/:id',
    component: ItemDetails,

},
{
    path: '/login',
    component: Login,

}
]